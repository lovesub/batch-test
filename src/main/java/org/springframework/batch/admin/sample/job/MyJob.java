package org.springframework.batch.admin.sample.job;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import kr.co.sportsone.batch.job.item.TestItemProcessor;
import kr.co.sportsone.batch.job.item.TestItemReader;
import kr.co.sportsone.batch.job.item.TestItemWriter;

@Configuration
public class MyJob {
	
	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;
	
	@Bean
	@JobScope
	public TestItemReader itemReader() {
		return new TestItemReader();
	}
	
	@Bean
	public TestItemProcessor itemProcessor(){
		return new TestItemProcessor();
	}
	
	@Bean
	@StepScope
	public TestItemWriter itemWriter() {
		TestItemWriter itemWriter = new TestItemWriter();
		return itemWriter;
	}
	
	@Bean
	public Step testStep1() {
		return stepBuilderFactory.get("testStep1")
				.<Integer, String>chunk(1)
				.reader(itemReader())
				.processor(itemProcessor())
				.writer(itemWriter())
				.build();
	}
	
	@Bean
	public Job testJob() {
		return jobBuilderFactory.get("testJob")
				.incrementer(new RunIdIncrementer())
				.start(testStep1())
				.build();
	}
	

}
