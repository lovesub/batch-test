package org.springframework.batch.admin.sample.job;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import kr.co.sportsone.batch.model.Account;
import kr.co.sportsone.batch.mybatis.repository.TestMapper;

@Configuration
@EnableScheduling
public class JobScheduler {

	private Logger logger = Logger.getLogger(JobScheduler.class);
	
	@Autowired
	private JobLauncher jobLauncher;
	
	@Autowired
	@Qualifier("testJob")
	private Job testJob;
	
//	@Autowired
//	private TestMapper testMapper;
	
	@Scheduled(fixedRate=10000)
	public void startJob() throws Exception{
		logger.info("startJob");
		
//		List<Account> accountList = testMapper.selectAccountList();
//		if(accountList.size() > 0){
//			System.out.println(accountList.get(0).getName());
//		}
		
		Map<String,JobParameter> parameterMap = new HashMap<>();
		parameterMap.put("date", new JobParameter(new Date()));
		
		JobParameters jobParameter = new JobParameters(parameterMap);
		
		JobExecution jobExcution = jobLauncher.run(testJob, jobParameter);
	}
}
