package kr.co.sportsone.batch.job.item;

import org.springframework.batch.item.ItemProcessor;

public class TestItemProcessor implements ItemProcessor<Integer, String>{

	@Override
	public String process(Integer item) throws Exception {
		return String.valueOf(item);
	}
}
