package kr.co.sportsone.batch.job.item;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemWriter;

public class TestItemWriter implements ItemWriter<String> {

	private static final Logger logger = Logger.getLogger(TestItemWriter.class);

	public void write(List<? extends String> items) throws Exception {
		for(String item : items){
			logger.info(item);
		}
	}
}
