package kr.co.sportsone.batch.mybatis.repository;

import java.util.List;

import kr.co.sportsone.batch.model.Account;

/**
 * 
 * DB 테스트용
 * 
 * @author	Hoon Jang
 * @since	2016-03-24
 */
public interface TestMapper {

	/**
	 * 
	 * 계정목록 조회
	 * 
	 * @param	map
	 * @return	계정목록
	 */
	public List<Account> selectAccountList();
}
