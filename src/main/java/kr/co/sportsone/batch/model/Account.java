package kr.co.sportsone.batch.model;

/**
 * 계정 정보
 * @author	Hoon Jang
 * @since	2016-03-24
 */
public class Account {
	
	/** 아이디	*/ private String id;
	/** 이름		*/ private String name;
	/** 나이		*/ private int age;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
}
