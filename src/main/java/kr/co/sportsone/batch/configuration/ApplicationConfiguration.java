package kr.co.sportsone.batch.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource("classpath:application.properties")
public class ApplicationConfiguration {
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	@Value("${db.driverclassname}")
	private String driverClassName;
	
	@Value("${db.url}") 
	private String dbUrl;

	@Value("${db.username}")
	private String dbUserName;

	@Value("${db.password}")
	private String dbPassword;
	
	@Value("${db.maxactive}")
	private int maxActive;
	
	@Value("${db.maxwait}")
	private int maxWait;
	
	@Value("${db.validationquery}")
	private String validationQuery;
	
	@Value("${db.testwhileidle}")
	private boolean testWhileIdle;
	
	@Value("${db.timebetweenevictionrunsmillis}")
	private int timeBetweenEvictionRunsMillis;

//	@Value("${spring.resourcehandler}")
//	private String resourceHandler;
//	
//	@Value("${spring.resourcelocations}")
//	private String resourceLocations;
//	
//	@Value("${spring.jsp.view.prefix}")
//	private String jspViewPrefix;
//	
//	@Value("${spring.jsp.view.suffix}")
//	private String jspViewSuffix;
//	
//	@Value("${spring.tiles.definitions}")
//	private String tilesDefinitions;
			
	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public int getMaxActive() {
		return maxActive;
	}

	public void setMaxActive(int maxActive) {
		this.maxActive = maxActive;
	}

	public int getMaxWait() {
		return maxWait;
	}

	public void setMaxWait(int maxWait) {
		this.maxWait = maxWait;
	}
	
	public String getValidationQuery() {
		return validationQuery;
	}

	public void setValidationQuery(String validationQuery) {
		this.validationQuery = validationQuery;
	}

	public boolean getTestWhileIdle() {
		return testWhileIdle;
	}

	public void setTestWhileIdle(boolean testWhileIdle) {
		this.testWhileIdle = testWhileIdle;
	}

	public int getTimeBetweenEvictionRunsMillis() {
		return timeBetweenEvictionRunsMillis;
	}

	public void setTimeBetweenEvictionRunsMillis(int timeBetweenEvictionRunsMillis) {
		this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
	}
	
	public String getDbUrl() {
		return dbUrl;
	}

	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}
	
	public String getDbUserName() {
		return dbUserName;
	}

	public void setDbUserName(String dbUserName) {
		this.dbUserName = dbUserName;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}
	
//	public String getResourceHandler() {
//		return resourceHandler;
//	}
//
//	public void setResourceHandler(String resourceHandler) {
//		this.resourceHandler = resourceHandler;
//	}
//
//	public String getResourceLocations() {
//		return resourceLocations;
//	}
//
//	public void setResourceLocations(String resourceLocations) {
//		this.resourceLocations = resourceLocations;
//	}
//	
//	public String getJspViewPrefix() {
//		return jspViewPrefix;
//	}
//
//	public void setJspViewPrefix(String jspViewPrefix) {
//		this.jspViewPrefix = jspViewPrefix;
//	}
//
//	public String getJspViewSuffix() {
//		return jspViewSuffix;
//	}
//
//	public void setJspViewSuffix(String jspViewSuffix) {
//		this.jspViewSuffix = jspViewSuffix;
//	}
//
//	public String getTilesDefinitions() {
//		return tilesDefinitions;
//	}
//
//	public void setTilesDefinitions(String tilesDefinitions) {
//		this.tilesDefinitions = tilesDefinitions;
//	}

}