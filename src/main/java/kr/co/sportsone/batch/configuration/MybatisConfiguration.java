package kr.co.sportsone.batch.configuration;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@MapperScan(basePackages="kr.co.sportsone.batch.mybatis.repository")
public class MybatisConfiguration {

	@Autowired 
	private ApplicationConfiguration applicationConfiguration;
	
	@Bean
    public DataSource dataSource() {

		System.out.println("DATASOURCE INIT #############################################################################################################################################");
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(applicationConfiguration.getDriverClassName());
		dataSource.setUrl(applicationConfiguration.getDbUrl());
		dataSource.setUsername(applicationConfiguration.getDbUserName());
		dataSource.setPassword(applicationConfiguration.getDbPassword());
		dataSource.setMaxActive(applicationConfiguration.getMaxActive());
		dataSource.setMaxWait(applicationConfiguration.getMaxWait());
		dataSource.setValidationQuery(applicationConfiguration.getValidationQuery());
		dataSource.setTestWhileIdle(applicationConfiguration.getTestWhileIdle());
		dataSource.setTimeBetweenEvictionRunsMillis(applicationConfiguration.getTimeBetweenEvictionRunsMillis());
        return dataSource;
        
    }
	
    @Bean
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }
    
	@Bean
	public SqlSessionFactory sqlSessionFactoryBean() throws Exception {

		System.out.println("SQLSESSIONFACTORY INIT #############################################################################################################################################");
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(dataSource());
		sqlSessionFactoryBean.setTypeAliasesPackage("kr.co.sportsone.batch.model");
		sqlSessionFactoryBean.setConfigLocation(new DefaultResourceLoader().getResource("classpath:mybatis/config/MybatisConfiguration.xml"));
		sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mybatis/sql/*.xml"));
		return sqlSessionFactoryBean.getObject();
		
	}
}
